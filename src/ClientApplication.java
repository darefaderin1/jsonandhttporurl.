import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;


public class ClientApplication {
    static String serverURL = "http://localhost:82/JSON_using_HTTP";

    public static void main(String[] args) {
        sendHTTPRequest();
    }

    private static void sendHTTPRequest() {
        CloseableHttpClient httpClient = null;
        HttpGet httpRequest = null;
        CloseableHttpResponse httpResponse = null;
        String result = null;
        try {
            httpClient = HttpClients.createDefault();
            httpRequest = new HttpGet(serverURL);
            httpResponse = httpClient.execute(httpRequest);
            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {
                result = EntityUtils.toString(entity);
                if (result != null){
                    ObjectMapper mapper = new ObjectMapper();
//                    System.out.println(result);
                    PersonObject personObject = mapper.readValue(result, PersonObject.class);
                    System.out.println("Result from server: "+personObject.toString());
                }
            }
            httpResponse.close();
            httpClient.close();
        } catch (IOException ioException) {
            System.out.println("An exception occur when reaching the URL");
        }
    }
}
