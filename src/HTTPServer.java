import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class HTTPServer {

    public static void main(String[] arg) {
        try {
            HttpServer FaderinServer = HttpServer.create(new InetSocketAddress("localhost", 82), 0);
            FaderinServer.createContext("/JSON_using_HTTP", new FaderinHTTPHandler());
            ThreadPoolExecutor multitask = (ThreadPoolExecutor) Executors.newFixedThreadPool(8);
            FaderinServer.setExecutor(multitask);
            FaderinServer.start();
            System.out.println("Faderin Server Started");
        } catch (IOException ioException) {
            System.out.println("I/O exception encountred while creating HTTP server");
        }
    }


    private static class FaderinHTTPHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            System.out.println("Request received by server, waiting for server to respond. Request method is " + exchange.getRequestMethod());
            OutputStream FaderinServerResponse = exchange.getResponseBody();
            if (exchange.getRequestMethod().equals("GET")) {
                System.out.println("Faderin server is handling the GET request");
                String successMessage = "";

                ObjectMapper jsonMapper = new ObjectMapper();

                PersonObject manager  = new PersonObject();
                manager.setName("Omotoso");
                manager.setAge(32);
                manager.setEyeColor("Brown");
                manager.setHairColor("Black");
                manager.setHeight(5.8);
                manager.setWeight(67.9);

                successMessage = jsonMapper.writeValueAsString(manager);


                exchange.sendResponseHeaders(200,successMessage.length());
                FaderinServerResponse.write(successMessage.getBytes(StandardCharsets.UTF_8));

                FaderinServerResponse.flush();
                FaderinServerResponse.close();
            } else {
                String unsupportedRequest = "Faderin server does not support this request";
                System.out.println(unsupportedRequest);
                exchange.sendResponseHeaders(501,unsupportedRequest.length());
                FaderinServerResponse.write(unsupportedRequest.getBytes(StandardCharsets.UTF_8));

                FaderinServerResponse.flush();
                FaderinServerResponse.close();

            }

        }
    }
}
